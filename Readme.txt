I/ Project Installations

    1   run "composer install"
    2   run "npm install"(install package require for project, it will look for dependencies in package.json file)
    3   run "npm run watch or dev or prod" 
        -   watch: in case, you want to change some files and push npm to build project(in development)
        -   dev: in case, you want to build project in development mode
        -   prod: in case, you want to build project in production mode

        Note: this project is created with npm (V3.5.2) and Node (V8.10.0)

    4   run "php artisan db:seed" to Create default user with assigned roles and permission in database record
    5   run "php artisan db:seed --class=ContentTypeSeeder" to Create default Content Types in database record
