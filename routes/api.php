<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::post('register', 'AuthController@register');
// Route::post('login', 'AuthController@login');
// Route::get('refresh', 'AuthController@refresh');
// Route::group(['middleware' => 'auth:api'], function(){

//     Route::post('logout', 'AuthController@logout');

    
//     Route::group(['prefix'=>'user'], function(){
//         Route::get('/all','UserController@getAllusers');
//         Route::get('/','UserController@getUser');
//         Route::post('/','UserController@store');
//         Route::get('/roles','UserController@getUserRoles');
//         Route::post('/update','UserController@update');
//         Route::post('/change-password','UserController@changePassword');
//         Route::post('/block-unblock','UserController@block');

//     });
//     Route::get('/allroles', 'UserController@getAllRoles');

    
// });
Route::group(['middleware' => 'jwt.auth'], function(){

    Route::group(['prefix'=>'content'], function(){
        Route::get('/search-filter','API\ContentController@searchFilter')->middleware('permission:read-Contents');
        

    });
});    