<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

Route::group(['middleware' => 'restrict.ip'], function(){

    Route::get('/', function (Request $request) {
        // dd(ini_get('upload_max_filesize'),ini_get('post_max_size'),ini_get('max_file_uploads'));
        
        return view('index');
    });

    // Auth::routes();
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

    // Route::get('/home', 'HomeController@index')->name('home');

    // Route::post('auth/register', 'AuthController@register');
    // Route::post('auth/login', 'AuthController@login');
    // Route::get('auth/refresh', 'AuthController@refresh');
    // Route::group(['middleware' => 'auth'], function(){
    //     Route::get('auth/user', 'AuthController@user');
    //     Route::post('auth/logout', 'AuthController@logout');
    // });

    // Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::get('refresh', 'AuthController@refresh');
    Route::get('setting/get-app-name','SettingController@getAppName');
    Route::group(['middleware' => 'auth:api'], function(){

        Route::post('logout', 'AuthController@logout');

        
        Route::group(['prefix'=>'user'], function(){
            Route::get('/all','UserController@getAllusers')->middleware('permission:read-Users');
            Route::get('/','UserController@getUser')->middleware('permission:read-Users');
            Route::post('/','UserController@store')->middleware('permission:create-Users');
            Route::get('/roles','UserController@getUserRoles');
            Route::post('/update','UserController@update')->middleware('permission:update-Users');
            Route::post('/change-password','UserController@changePassword')->middleware('permission:update-Users');
            Route::post('/block-unblock','UserController@block')->middleware('permission:update-Users');
            Route::get('/get-all','UserController@getUsers');

        });
        Route::get('/allroles', 'UserController@getAllRoles');

        Route::group(['prefix'=>'role'], function(){
            
            Route::get('/','RoleController@index')->middleware('permission:read-Roles');
            Route::get('/show','RoleController@show')->middleware('permission:read-Roles');
            Route::post('/','RoleController@store')->middleware('permission:create-Roles');
            Route::post('/update','RoleController@update')->middleware('permission:update-Roles');
            Route::post('/delete','RoleController@delete')->middleware('permission:delete-Roles');
            Route::get('/permissions','RoleController@permission')->middleware('permission:read-Roles');
            Route::post('/update/permission','RoleController@updatePermission')->middleware('permission:update-Roles');
            Route::get('/all-acls','RoleController@getAllACLs');


        });

        Route::group(['prefix'=>'acls'], function(){
            Route::get('/','ACLsController@index')->middleware('permission:read-ACLs');
            Route::get('/show','ACLsController@show')->middleware('permission:read-ACLs');
            Route::post('/','ACLsController@store')->middleware('permission:create-ACLs');
            Route::post('/update','ACLsController@update')->middleware('permission:update-ACLs');
            Route::post('/delete','ACLsController@delete')->middleware('permission:delete-ACLs');
        
        });

        Route::group(['prefix'=>'content'], function(){
            Route::get('/','ContentController@index')->middleware('permission:read-Contents');
            Route::get('/show','ContentController@show')->middleware('permission:read-Contents');
            Route::post('/','ContentController@store')->middleware('permission:create-Contents');
            Route::post('/update','ContentController@update')->middleware('permission:update-Contents');
            Route::post('/delete','ContentController@delete')->middleware('permission:delete-Contents');
            Route::get('/all','ContentController@getTotalContents');

        });

        Route::group(['prefix'=>'content-type'], function(){
            Route::get('/','ContentTypeController@index')->middleware('permission:read-ContentTypes');
            Route::get('/show','ContentTypeController@show')->middleware('permission:read-ContentTypes');
            Route::post('/','ContentTypeController@store')->middleware('permission:create-ContentTypes');
            Route::post('/update','ContentTypeController@update')->middleware('permission:update-ContentTypes');
            Route::post('/delete','ContentTypeController@delete')->middleware('permission:delete-ContentTypes');
            Route::get('/all','ContentTypeController@getAllContentTypes');

            
        });

        Route::group(['prefix'=>'tag'], function(){
            Route::get('/','TagController@index')->middleware('permission:read-Tags');
            Route::get('/show','TagController@show')->middleware('permission:read-Tags');
            Route::post('/','TagController@store')->middleware('permission:create-Tags');
            Route::post('/update','TagController@update')->middleware('permission:update-Tags');
            Route::post('/delete','TagController@delete')->middleware('permission:delete-Tags');
            Route::get('/all','TagController@getAllTags');

        });

        Route::group(['prefix'=>'token'], function(){
            Route::get('/','TokenController@index')->middleware('admin');
            Route::post('/','TokenController@store')->middleware('admin');

        });
        
        Route::group(['prefix'=>'setting'], function(){

            Route::post('/update-app-name','SettingController@changeAppName')->middleware('admin');
        
        });
        Route::get('/server-upload-specs',function ()
        {
            $data = [
                'upload_max_filesize' => ini_get('upload_max_filesize'),
                'post_max_size'=>ini_get('post_max_size'),
                'max_file_uploads'=>ini_get('max_file_uploads'),
            ];
            return response()->json([
                'status'=> 'success',
                'datas'=> $data,
            ],200);
        });
        Route::get('/get-user-permissions','RoleController@getUserPermission');

        Route::get('/get-documen-file',function (Request $request)
        {
            # code...
            $path = $request->path;

            $path = ltrim($path, '/');

            $handler = new \Symfony\Component\HttpFoundation\File\File($path);

            $header_content_type = $handler->getMimeType();
            $header_content_size = $handler->getSize();
            $header_content_name = $handler->getFilename();

            $headers = array(
                'Content-Name' => $header_content_name,
                'Content-Type' => $header_content_type,
                'Content-Size' => $header_content_size,
            );

            return response()->file($path,$headers);
        });

        
    });
});    