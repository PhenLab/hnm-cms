<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('desc')->nullable();
            $table->timestamps();
            $table->boolean('attachment')->default(0);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();

        });
    }
    	
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_types');
    }
}
