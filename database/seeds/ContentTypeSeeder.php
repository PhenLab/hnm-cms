<?php

use App\ContentType;
use Illuminate\Database\Seeder;

class ContentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        ContentType::firstOrCreate([
            'title'=> 'image',
            'attachment'=> 1,
            'desc'=>'can attact only image'

        ]);
        
        ContentType::firstOrCreate([
            'title'=> 'video',
            'attachment'=> 1,
            'desc'=>'can attact only video'

        ]);
        ContentType::firstOrCreate([
            'title'=> 'file',
            'attachment'=> 1,
            'desc'=>'can attact only file'

        ]);

    }
}
