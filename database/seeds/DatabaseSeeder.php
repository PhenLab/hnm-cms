<?php

use App\Acls;
use App\Role;
use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
       
        $admin = User::firstOrNew([
            'name' => 'Admin',
            'email' => 'admin@hnmcms.com',
        ]);
        if(!$admin->id){
            $admin->password = Hash::make('admin123');
        }
        $admin->save();

        $user = User::firstOrNew([
            'name' => 'User',
            'email' => 'user@hnmcms.com',
        ]);
        if(!$user->id)
            $user->password = Hash::make('user123');
        
        $user->save();

        $roles =  array();

        $roles[] = Role::firstOrCreate([
            'code'=> 'ADM',
            'name'=> 'Administrator',
            'desc'=>'created by db seeder'

        ]);
        
        $roles[] = Role::firstOrCreate([
            'code'=> 'ACCT',
            'name'=> 'Accountant',
            'desc'=>'created by db seeder'

        ]);
        $roles[] = Role::firstOrCreate([
            'code'=> 'SNTR',
            'name'=> 'Sentry',
            'desc'=>'created by db seeder'
        ]);

        $acls = [];

        $acls[] = Acls::firstOrCreate([
            'name' => 'Contents',
            'desc' => 'CRUD Contents'
        ]);

        $acls[] = Acls::firstOrCreate([
            'name' => 'ContentTypes',
            'desc' => 'CRUD Content Types'
        ]);

        $acls[] = Acls::firstOrCreate([
            'name' => 'Tags',
            'desc' => 'CRUD Tags'
        ]);

        $acls[] = Acls::firstOrCreate([
            'name' => 'Users',
            'desc' => 'CRUD user'
        ]);

        $acls[] = Acls::firstOrCreate([
            'name' => 'Roles',
            'desc' => 'CRUD role'
        ]);

        $acls[] = Acls::firstOrCreate([
            'name' => 'ACLs',
            'desc' => 'CRUD Permission'
        ]);  
        $acls[] = Acls::firstOrCreate([
            'name' => 'Dashboard',
            'desc' => 'Showing Dashboard'
        ]);

        
        foreach($roles as $role){        
            if($role->code == 'ADM'){
                $role->acls()->syncWithoutDetaching(
                    [$acls[0]->id=>[
                        'create' => 1,
                        'read' => 1,
                        'update' => 1,
                        'delete' => 1,
                        'created_at' => Carbon::now()
                    ],
                    $acls[1]->id=>[
                        'create' => 1,
                        'read' => 1,
                        'update' => 1,
                        'delete' => 1,
                        'created_at' => Carbon::now()
                    ],
                    $acls[2]->id=>[
                        'create' => 1,
                        'read' => 1,
                        'update' => 1,
                        'delete' => 1,
                        'created_at' => Carbon::now()
                    ],
                    $acls[3]->id=>[
                        'create' => 1,
                        'read' => 1,
                        'update' => 1,
                        'delete' => 1,
                        'created_at' => Carbon::now()
                    ],
                    $acls[4]->id=>[
                        'create' => 1,
                        'read' => 1,
                        'update' => 1,
                        'delete' => 1,
                        'created_at' => Carbon::now()
                    ],
                    $acls[5]->id=>[
                        'create' => 1,
                        'read' => 1,
                        'update' => 1,
                        'delete' => 1,
                        'created_at' => Carbon::now()
                    ],
                    $acls[6]->id=>[
                        'create' => 1,
                        'read' => 1,
                        'update' => 1,
                        'delete' => 1,
                        'created_at' => Carbon::now()
                    ],
                    ]
                );
            }
            
            $admin->roles()->syncWithoutDetaching($role->id);

            if($role->code != 'ADM'){
                $user->roles()->syncWithoutDetaching($role->id);
            }

        }
       
    }
}
