<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>HNM-CMS</title>
  </head>
  <body>
    <div id="appMain">
    </div>
    <script src="{{asset('template/js/main.js')}}">
    </script>
    
  </body>
</html>
