import Vue from 'vue'
import App from './App.vue'
import store from './store'
import VueRouter from 'vue-router';
import { routes} from './router/routes';
import { index } from './components/index';
import jQuery from 'jquery';
import axios from 'axios'
import VueAuth from '@websanova/vue-auth'
import VueAxios from 'vue-axios'
import ApiRequest from './api/api-request'
import VueFlashMessage from 'vue-flash-message';
import VuejsDialog from 'vuejs-dialog';
import CxltToastr from 'cxlt-vue2-toastr'
import middlewarePipeline from './router/middlewares/middlewarePipeline';

//controller as mixin 
import {UserControllerMixin} from './controllers/UserController';
import {RoleControllerMixin} from './controllers/RoleController';
import {ContentControllerMixin} from './controllers/ContentController';
import VueUploadMultipleImage from 'vue-upload-multiple-image';
import Multiselect from 'vue-multiselect'
import VueCryptojs from 'vue-cryptojs'
// Import loading indicator component
import Loading from 'vue-loading-overlay';
//config file
import configurations from '../template/configs/conf'
import {DashboardControllerMixin} from './controllers/DashboardController';
import moment from 'moment'
import CoolLightBox from 'vue-cool-lightbox'
const nl2br  = require('nl2br');
import VModal from 'vue-js-modal'

 
Vue.use(VueCryptojs)


var toastrConfigs = {
    position: 'bottom right',
}
Vue.use(CxltToastr, toastrConfigs)  
// Tell Vue to install the plugin.
Vue.use(VuejsDialog);

Vue.component('vue-upload-multiple-image',VueUploadMultipleImage)
Vue.component( 'multiselect',  Multiselect  )

const config = {
	name: 'flashMessage',
	tag: 'FlashMessage',
	time: 4000
};

Vue.use(VueFlashMessage, config);
Vue.component('pagination', require('laravel-vue-pagination'));

var loadingConfig = {color:'#007bff',loader:'dots'};
// Init loading indicator  plugin
Vue.use(Loading, loadingConfig);

//init image lightbox plugin
Vue.use(CoolLightBox);
Vue.use(VModal,  {  dynamic: true, dynamicDefaults: { clickToClose: false } })

window.jQuery = jQuery;
window.$ = jQuery;

// Set Vue globally
window.Vue = Vue


// Set Router for each page 
Vue.use(VueRouter);


const router = new VueRouter({
    routes,
    linkActiveClass: 'open active',
    scrollBehavior: () => ({ y: 0 }),
    mode: 'hash'
});


//prevent route from unauthorized user
const secretPassKey = configurations['secretPassKey'];
const vueInstance = Vue;
var token = localStorage.getItem('token');
if(token != null)
   token = Vue.CryptoJS.AES.decrypt(token, secretPassKey).toString(Vue.CryptoJS.enc.Utf8)


router.beforeEach((to, from, next, transition) => {

  var authPath = ['/auth/register','/auth/login','/auth/forget-password','/password/reset']
  if (to.path === '/Page404') 
    store.dispatch('page_error', true);

  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!store.getters.isLoggedIn  ) {

      next({name:'login'})
    } else {

      //implement middleware
      if (!to.meta.middleware) {
        return next()
      }
      const middleware = to.meta.middleware
      var module = middleware[0][1];

      const context = {
          to,
          from,
          next,
          store,
          router,
          module,
          vueInstance,
          secretPassKey,
          

      }

      // var routeName = ''
      
      return middleware[0][0]({
          ...context,
          next: middlewarePipeline(context, middleware, 1)

      })
    }
  }
  else if(authPath.includes(to.path) && store.getters.isLoggedIn){
    next({name:'home'})
  }
  else {
    next() // does not require auth, make sure to always call next()!
  }
})


if (token) 
  axios.defaults.headers.common['Authorization'] = token;
Vue.use(VueAxios, axios)
Vue.use(VueAuth, ApiRequest)
Vue.router =  router;
var systemName = 'HNM CMS'
function XHRequest(method = 'GET', url = '', async = false, token = '', callBackFunction) {
  var xhr = new XMLHttpRequest();
  xhr.open(method, url , async); // your url will pass to open method

  xhr.setRequestHeader('Authorization', token);
  xhr.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
          var data = JSON.parse(this.response);
          if (typeof callBackFunction === 'function') callBackFunction(data);
      }
  };
  xhr.send();
}

XHRequest('GET','/setting/get-app-name',false, token, function(response) {
  
  try {
    systemName = response.name.content;
  } catch (error) {
    console.log(error)
  }

});
//change page title 
document.title = systemName

export const  globalMixin = {
  
  data(){
    return{
      default_profile: '/images/default_profile.png',
      maxNumberOfUplodImg:50,
      megabyteInBytes:1048576,
      secretPassKey: secretPassKey,
      base_url: window.location.origin,
      itemPerPage:20,
      systemName:systemName

    }
  },
  methods: {
    async getRoleByUserID(id = 0){
      var params = new FormData();
      params.set('id',id);
      this.$http(
        {
        method: 'get',
        url: 'user/roles?id='+id,
        // params
        })
        .then(
        res => {
            var roles = res.data.roles;
            return roles;

        }
        ).catch(
        err => {
            console.log(err);
        }
        )
    },
    getDefaultProfile(){
      return "/images/default_profile.png";
    },

    onLoadImageError(){
      event.target.src = this.getDefaultProfile();

    },
    deleteData(id =  0, module,url){
      
      const thisInstance = this;
      var permissions = this.CryptoJS.AES.decrypt(localStorage.getItem('rolePerms'), this.secretPassKey).toString(this.CryptoJS.enc.Utf8) 
      permissions = JSON.parse(permissions);

      var data = {
        url: url,
        id:id,
      };
      
      if(permissions[module] === true){ 
      
        return this.$dialog.confirm('Are you sure you want to delete? ')
        
        .then(function (res) {
          // bus.$emit('deletedData', data ); 
          var formData = new FormData();       
          formData.append('id',data.id);

          return thisInstance.$http.post(data.url,formData)
            .then(
                res=>{
                  thisInstance.$root.$emit(module, true, res.data);
                  // EventBus.$emit('deleteDataResult', true);    
                }
            )
            .catch(
                err=>{
                    console.log(err)
                    thisInstance.$root.$emit(module, false ,err.response.data);
                    // EventBus.$emit('deleteDataResult', false,err.response.data);    
                }
            )

        })
        .catch(function (e) {
            console.log(e)
            
        });
      }     
      else{
        store.dispatch('page_error', true)
        router.push({name:'permissiondenied'})
      }
        
    },
    getFlashMsgDuration(){
      return 5000;
    },

    getAllTags(){
      return this.$http(
          {
          method: 'get',
          url: '/tag/all',
          })
          .then(
          res => {
              return res.data.datas;
              
          }
          ).catch(
          err => {
              console.log('request content type error');
              
          }
          )
    },
    getServeruploadSpecs(){
      return this.$http(
          {
          method: 'get',
          url: '/server-upload-specs',
          })
          .then(
          res => {
              return res.data.datas;
              
          }
          ).catch(
          err => {
              console.log('request server-upload-specs error');
              
          }
          )
    },

    getEncryptedText(plainText){
      return this.CryptoJS.AES.encrypt(plainText, this.secretPassKey).toString()
    },
    getDecryptedText(cipher){
      return this.CryptoJS.AES.decrypt(cipher, this.secretPassKey).toString(this.CryptoJS.enc.Utf8)
    },
    showAlertMsg(type = 'flash', title = 'Permission'){
        this.success =  localStorage.getItem('success');
        if (this.success === 'true') {
            localStorage.setItem('success', false);     
            if(type === 'flash'){
              this.flash(localStorage.getItem('msg'), 'success', {
                      timeout: this.getFlashMsgDuration(),
                  });
            }   
            else if (type === 'toast'){
                this.$toast.success({
                        title:title,
                        message:localStorage.getItem('msg'),
                    })

            }
            
        }
    },
    updatePermission(actions = [], module = '', status = false){
        
        var permissions = JSON.parse( this.getDecryptedText(localStorage.getItem('rolePerms') ) )
        actions.forEach(element => {
            permissions[element+module] = status;
            
        });
        localStorage.setItem('rolePerms', this.getEncryptedText( JSON.stringify(permissions)))
    },
    getDocumentFile(path){
      
      return this.$http.get('/get-documen-file?path='+path,
        {
          responseType: 'arraybuffer'
        })
        .then(
        res => {
            return res;
        }
        ).catch(
        err => {
            console.log('request content type error');
            
        }
        )
    },
    isFileImage(file) {
      return file && file['type'].split('/')[0] === 'image';
    },
    formatDate(date = new Date(), format = 'DD-MM-YYYY HH:mm:ss' ){
      if (date != null) {
        date = moment(date).format(format);
      }
      return date;
    },
    cutMultipleCharacterText(text, character = 100){
      if (text != null && text.length > character)
        text = text.substring(0,character) + '...';

      return text;  
    },
    // show text with newline(it can be use with v-html Ex: <p v-html="nl2br(text)"></p> ) )
    vnl2br(text, is_enter ){
      var htmlText = ''
      if(text != null)
          htmlText = nl2br(text)
      if(is_enter === true){
          htmlText +='<br>'
      }
      return htmlText
    }
  
  },
  created(){

    
  },
  mounted(){
      //Initialize Select2 Elements
      $(".select2").select2();
  },
  
}
  
const AllMyMixin  = Vue.extend({
  mixins:[globalMixin,UserControllerMixin,RoleControllerMixin, ContentControllerMixin, DashboardControllerMixin]
})

Vue.mixin(AllMyMixin );

///DOWNLOAD FILES 
jQuery(document).on('click','.downloadFile', function(e) {
  e.preventDefault();
  var file_path = $(this).data('filepath') 
  axios.get('/get-documen-file?path='+file_path,
      {
      responseType: 'arraybuffer'
      })
      .then(
      res => {
          var blob = new Blob([res.data],
                      {
                          type:res.headers['content-type'],
                      });
          blob.lastModifiedDate = new Date();            
          blob.name = res.headers['content-name']

          var fileURL = window.URL.createObjectURL(blob);
          var fileLink = document.createElement('a');

          fileLink.href = fileURL;
          fileLink.setAttribute('download', blob.name);
          document.body.appendChild(fileLink);

          fileLink.click();
      }
      ).catch(
      err => {
          console.log(err)
          
      }
      )
  
})
//clear active menu 
localStorage.setItem('activeMenu', null)
new Vue({
  el: '#appMain',
  router,
  store,
  render: h => h(App),
  components: { App },
  
})
