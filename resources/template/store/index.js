import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios'
import app from './modules/app';

import * as getters from './getters';

Vue.use(Vuex);

const store = new Vuex.Store({
  strict: false,  // process.env.NODE_ENV !== 'production',
  getters,
  modules: {
    app
  },
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
    user:{},
    hasPageError: false,

  },
  mutations: {
    auth_request(state){
      state.status = 'loading'
    },
    auth_success(state, token, user){
      state.status = 'success'
      state.token = token
      state.user = user
    },
    auth_error(state){
      state.status = 'error'
    },
    logout(state){
      state.status = ''
      state.token = ''
    },
    page_error(state, status){
      state.hasPageError = status;
    },
    
  },
  actions:{
    login({commit}, user){
      return new Promise((resolve, reject) => {
        commit('auth_request')
        axios({url: '/login', data: user, method: 'POST' })
        .then(resp => {
          if(resp.data.status === 'success'){
            console.log('vuex_login_success');
            const token ='Bearer '+resp.data.token;
            
            const user = resp.data.user       
            localStorage.setItem('token', token)
            localStorage.setItem('user',JSON.stringify(user))
            axios.defaults.headers.common['Authorization'] =  token;
            commit('auth_success', token, user)
            resolve(resp)
          }
          else{
            console.log('vuex_login_error');
            commit('auth_error')
            localStorage.removeItem('token')
            reject(resp)
          }
        })
        .catch(err => {
          console.log('vuex_login_error');
          commit('auth_error')
          localStorage.removeItem('token')
          reject(err)
        })
      })
    },
    register({commit}, user){
      return new Promise((resolve, reject) => {
        commit('auth_request')
        axios({url: '/register', data: user, method: 'POST' })
        .then(resp => {
          const token ='Bearer ' + concat(resp.data.token)
          const user = resp.data.user
          localStorage.setItem('token', token)
          axios.defaults.headers.common['Authorization'] = token
          commit('auth_success', token, user)
          resolve(resp)
        })
        .catch(err => {
          commit('auth_error', err)
          localStorage.removeItem('token')
          reject(err)
        })
      })
    },
    logout({commit}){
      return new Promise((resolve, reject) => {
        commit('logout')
        localStorage.clear()
        delete axios.defaults.headers.common['Authorization']
        resolve()
      })
    },
    page_error({commit}, status){
      commit('page_error', status);
    },
    
  },
  getters:{
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
    hasPageError: state => state.hasPageError,
  },
  logout({commit}){
    return new Promise((resolve, reject) => {
      commit('logout') 
      console.log('logout')
      localStorage.removeItem('token')
      delete axios.defaults.headers.common['Authorization']
      resolve()
    })
  }
  
  
})

export default store