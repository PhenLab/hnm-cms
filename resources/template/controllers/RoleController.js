
export const RoleControllerMixin = {
  data(){
    return{
    }
  },
  methods: {

    getRoleByUserID(id = 0){
      var params = new FormData();
      params.set('id',id);
      return this.$http(
        {
        method: 'get',
        url: 'user/roles?id='+id,
        // params
        })
        .then(
        res => {
            var roles = res.data.roles;
            return roles;

        }
        ).catch(
        err => {
            console.log('get role by user id error');
        }
        )
    },
    getAllRoles(){
      return this.$http(
          {
          method: 'get',
          url: '/allroles',
          })
          .then(
          res => {
              console.log('get role success')
              return res.data.roles;
              
          }
          ).catch(
          err => {
              console.log('request role error');
              
          }
          )
    },
    getAllACLs(){
        
      return this.$http.get('/role/all-acls')
            .then(
                res=>{
                    return res;
                }
            )
            .catch(
                err=>{
                    console.log(err)
                }
            )       
    },
    
    
  }
}