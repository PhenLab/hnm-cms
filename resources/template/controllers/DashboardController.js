
export const DashboardControllerMixin = {
  data(){
    return{
      
    }
  },
  methods: {

    getTotalContentTypes(){
      return this.$http(
          {
          method: 'get',
          url: '/content-type?is_paginate=false',
          })
          .then(
          res => {
              return res.data.datas;
              
          }
          ).catch(
          err => {
              console.log(err);
              
          }
          )
    },
    getTotalUsers(){
      return this.$http(
          {
          method: 'get',
          url: '/user/all?is_paginate=false',
          })
          .then(
          res => {
              return res.data.users;
              
          }
          ).catch(
          err => {
              console.log(err);
              
          }
          )
    },
    getTotalContents(){
      return this.$http(
          {
          method: 'get',
          url: '/content?is_paginate=false',
          })
          .then(
          res => {
              return res.data.datas;
              
          }
          ).catch(
          err => {
              console.log(err);
              
          }
          )
    },
    getTotalTags(){
      return this.$http(
          {
          method: 'get',
          url: '/tag?is_paginate=false',
          })
          .then(
          res => {
              return res.data.datas;
              
          }
          ).catch(
          err => {
              console.log(err);
              
          }
          )
    },
    
  },
  created(){
    
  }
}