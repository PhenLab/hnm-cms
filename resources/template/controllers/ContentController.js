
export const ContentControllerMixin = {
  data(){
    return{
      upload_max_filesize:' Upload Max Each File Size: ',
      post_max_size:' Post Max Size: ',
      max_file_uploads:' Max File Upload: ',
    }
  },
  methods: {

    getRoleByUserID(id = 0){
      var params = new FormData();
      params.set('id',id);
      return this.$http(
        {
        method: 'get',
        url: 'user/roles?id='+id,
        // params
        })
        .then(
        res => {
            var roles = res.data.roles;
            return roles;

        }
        ).catch(
        err => {
            console.log('get role by user id error');
        }
        )
    },
    getAllContentTypes(){
      return this.$http(
          {
          method: 'get',
          url: '/content-type/all',
          })
          .then(
          res => {
              return res.data.datas;
              
          }
          ).catch(
          err => {
              console.log('request content type error');
              
          }
          )
    },
    createObjectURL(f){
      return URL.createObjectURL(f)
    },
    
    onUploadVideo(event){
      var files = event.target.files;
      if (files.length === 1) {
          this.currentVideo = files[0].name
      }
      for (var i = 0; i < files.length; i++) {
          var f = files[i];
          // Only process video files.
          if (!f.type.match('video.*')) {
              
              continue;
          }

          this.videoUploaded.push(f);
      }
    },
    onUploadImage(event){
        var files = event.target.files;
        if (files.length === 1) {
            this.currentImage = files[0].name
        }
        for (var i = 0; i < files.length; i++) {
            var f = files[i];
            // Only process image files.
            if (!this.isFileImage(f)) {
                continue;
            }
            this.imageUploaded.push(f);
        }
    },
    onUploadFile(event){
        var files = event.target.files;
        if (files.length === 1) {
            this.currentFile = files[0].name
        }
        for (var i = 0; i < files.length; i++) {
            var f = files[i];
            this.fileUploaded.push(f);
        }
    },

    onDeleteImage(event, index){

        event.preventDefault();
        // var aTagRemove = $(event.target).parent()
        // var fileIndex = $(aTagRemove).data('fileid')
        var fileIndex = index
        
        var fileName = this.imageUploaded[fileIndex].name;
        this.deletedImageIndex.push(fileIndex);
        
        if( this.currentImage === fileName || this.imageUploaded.length === this.deletedImageIndex.length)
            $('input[name=imageFiles]').val('')
            
        // $(aTagRemove).parent().parent().parent().remove();
        $(event.target).closest('li').remove()

    },
    onDeleteVideo(event, index){

        event.preventDefault();
        // var aTagRemove = $(event.target).parent()
        // var fileIndex = $(aTagRemove).data('fileid')
        var fileIndex = index
        
        var fileName = this.videoUploaded[fileIndex].name;
        this.deletedVideoIndex.push(fileIndex);
        
        if( this.currentVideo === fileName || this.videoUploaded.length === this.deletedVideoIndex.length)
            $('input[name=videoFiles]').val('')
            
        // $(aTagRemove).parent().parent().parent().remove();
        $(event.target).closest('li').remove()
    },
    onDeleteFile(event, index){

        event.preventDefault();
        // var aTagRemove = $(event.target).parent()
        // var fileIndex = $(aTagRemove).data('fileid')
        var fileIndex = index

        var fileName = this.fileUploaded[fileIndex].name;
        this.deletedFileIndex.push(fileIndex);
        
        if( this.currentFile === fileName || this.fileUploaded.length === this.deletedFileIndex.length)
            $('input[name=files]').val('')
        $(event.target).closest('li').remove()
        // $(aTagRemove).parent().parent().parent().remove();
    },
    
      
      
  },
  created(){
    
  }, 
}