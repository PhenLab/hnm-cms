import VueCryptojs from 'vue-cryptojs'
import Vue from 'vue'
import configurations from '../configs/conf'
Vue.use(VueCryptojs)
     
const secretPassKey = configurations['secretPassKey'];
var token = localStorage.getItem('token');
if(token != null){
   token = Vue.CryptoJS.AES.decrypt(token, secretPassKey).toString(Vue.CryptoJS.enc.Utf8)
}
/**
 * UPDATE USER PERMMISSIONS
*/ 
var xhr = new XMLHttpRequest();
xhr.open("GET", '/get-user-permissions', false); // your url will pass to open method

xhr.setRequestHeader('Authorization', token);
xhr.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var permissions = JSON.parse(this.response);
      permissions = Vue.CryptoJS.AES.encrypt(JSON.stringify(permissions.permissions) , secretPassKey).toString()
      localStorage.setItem('rolePerms',permissions)
    }
  };
xhr.send();

var navItems = [];
var permissions =  localStorage.getItem('rolePerms');
if(permissions != null)
  permissions = JSON.parse( Vue.CryptoJS.AES.decrypt(permissions, 'hsecretnPasm@20').toString(Vue.CryptoJS.enc.Utf8) )

try {
    if ( permissions['readDashboard'] === true ) {

        navItems.push({
            name: 'Dashboard',
            url: '/dashboard',
            icon: 'fa fa-dashboard',
            
        });   
    }

    if ( permissions['readContents'] === true ||
        permissions['readTags'] === true ||
        permissions['readContentTypes'] === true){
        var ContentChild = []
        if (permissions['readContents'] === true) {
            ContentChild = [
            {   name: 'Contents',
                url: '/content/index',
                icon: 'fa fa-book',},
            ]
        }
        if (permissions['readContentTypes'] === true) {
            ContentChild.push(
            {   name: 'Content Types',
                url: '/content/content-type/index',
                icon: 'fa fa-filter',},
            )
        }
        if (permissions['readTags'] === true) {
            ContentChild.push(
            {   name: 'Tags',
                url: '/content/tag/index',
                icon: 'fa fa-tags'},
            )
        }
    
        navItems.push({
                    name: 'Contents',
                    icon: 'fa fa-file',
                    children:ContentChild
        })

    }


    if (permissions['readUsers'] === true ||
        permissions['readRoles'] === true ||
        permissions['readACLs'] === true  ||
        permissions['hasRoleAdmin'] === true){

        var settingChild = []
        if (permissions['readUsers'] === true) {
            settingChild = [
            {
                name: 'Users',
                url: '/securities/user',
                icon: 'fa fa-users'}
            ]
        }
        if (permissions['readRoles'] === true) {
            settingChild.push(
            {   name: 'Roles',
                url: '/securities/role/index',
                icon: 'fa fa-address-book'})
        }
        if (permissions['readACLs'] === true) {
            settingChild.push(
            {   name: 'ACLs',
                url: '/securities/acls/index',
                icon: 'fa fa-key'},)
        }
        if (permissions['hasRoleAdmin'] === true) {
            settingChild.push(
                {   name: 'Access Tokens',
                    url: '/securities/token/index',
                    icon: 'fa fa-key'
                },
            )
        }
        
        
        navItems.push({
                    name: 'Securities',
                    icon: 'fa fa-cogs',
                    children:settingChild}
        )

    }

} catch (error) {
    console.log(error);
}

export default {
    items: navItems
}

