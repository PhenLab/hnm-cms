import bearer from '@websanova/vue-auth/drivers/auth/bearer'
import axios from '@websanova/vue-auth/drivers/http/axios.1.x'
import router from '@websanova/vue-auth/drivers/router/vue-router.2.x'
// Auth base configuration some of this options
// can be override in method calls
const config = {
  auth: bearer,
  http: axios,
  router: router,
  tokenDefaultName: 'laravel-vue-spa',
  tokenStore: ['localStorage'],
  rolesVar: 'role',
  registerData: {url: 'auth/register', method: 'POST', redirect: '/dashboard'},
  loginData: {url: 'auth/login', method: 'POST', redirect:'/dashboard'},
  logoutData: {url: 'auth/logout', method: 'POST', redirect: '/', makeRequest: true},
  fetchData: {url: 'user', method: 'GET', enabled: true},
  refreshData: {url: 'refresh', method: 'GET', enabled: true, interval: 30},
  resetPasswordData: {url: 'password/email', method: 'POST'},

}
export default config

