export default function createPermission({ store ,next, to, router, module, secretPassKey, vueInstance }){
    
    var permissions = localStorage.getItem('rolePerms');
    if(permissions != null)
        permissions = JSON.parse( vueInstance.CryptoJS.AES.decrypt(permissions, secretPassKey).toString(vueInstance.CryptoJS.enc.Utf8) )

    if(permissions['create'+module] === true){
        store.dispatch('page_error', false);
        return next();

    }
    store.dispatch('page_error', true)
    return next({name:'permissiondenied'})
   }