export default function readPermission({ store ,next, to, router, module, vueInstance, secretPassKey }){
    
    var permissions = localStorage.getItem('rolePerms');


    permissions = JSON.parse( vueInstance.CryptoJS.AES.decrypt(permissions, secretPassKey).toString(vueInstance.CryptoJS.enc.Utf8) )

    if(permissions['read'+module] === true){
        store.dispatch('page_error', false);
        return next();

    }
    store.dispatch('page_error', true)
    return next({name:'permissiondenied'})
   }