// Dynamic Loading Modules

// Views
const Dashboard = resolve => { require.ensure(['../views/Dashboard.vue'], ()=>{ resolve(require('../views/Dashboard.vue')); }); };

// // User Info
const User = resolve => { require.ensure(['../layouts/User.vue'], ()=>{ resolve(require('../layouts/User.vue')); }); };
const UserIndex = resolve => { require.ensure(['../views/user/index.vue'], ()=>{ resolve(require('../views/user/index.vue')); }); };
const UserEdit = resolve => { require.ensure(['../views/user/edit.vue'], ()=>{ resolve(require('../views/user/edit.vue')); }); };
const UserCreate = resolve => { require.ensure(['../views/user/create.vue'], ()=>{ resolve(require('../views/user/create.vue')); }); };
const UserChangePassword = resolve => { require.ensure(['../views/user/change_password.vue'], ()=>{ resolve(require('../views/user/change_password.vue')); }); };

//Pages
const Login = resolve => { require.ensure(['../pages/login/Login.vue'], ()=>{ resolve(require('../pages/login/Login.vue')); }); };
const Register = resolve => { require.ensure(['../pages/register/Register.vue'], ()=>{ resolve(require('../pages/register/Register.vue')); }); };
const Page404 = resolve => { require.ensure(['../pages/Page404.vue'], ()=>{ resolve(require('../pages/Page404.vue')); }); };
const Page500 = resolve => { require.ensure(['../pages/Page500.vue'], ()=>{ resolve(require('../pages/Page500.vue')); }); };
const ForgetPassword = resolve => { require.ensure(['../pages/ForgetPassword.vue'], ()=>{ resolve(require('../pages/ForgetPassword.vue')); }); };
const NewPassword = resolve => { require.ensure(['../pages/new_password.vue'], ()=>{ resolve(require('../pages/new_password.vue')); }); };
const PermissionDeny = resolve => { require.ensure(['../pages/PagePermissionDeny.vue'], ()=>{ resolve(require('../pages/PagePermissionDeny.vue')); }); };


// // role Info
const RoleIndex = resolve => { require.ensure(['../views/role/index.vue'], ()=>{ resolve(require('../views/role/index.vue')); }); };
const RoleEdit = resolve => { require.ensure(['../views/role/edit.vue'], ()=>{ resolve(require('../views/role/edit.vue')); }); };
const RoleCreate = resolve => { require.ensure(['../views/role/create.vue'], ()=>{ resolve(require('../views/role/create.vue')); }); };
const RolePermision = resolve => { require.ensure(['../views/role/permissions.vue'], ()=>{ resolve(require('../views/role/permissions.vue')); }); };

// // ACLs Info
const ACLsIndex = resolve => { require.ensure(['../views/acls/index.vue'], ()=>{ resolve(require('../views/acls/index.vue')); }); };
const ACLsEdit = resolve => { require.ensure(['../views/acls/edit.vue'], ()=>{ resolve(require('../views/acls/edit.vue')); }); };
const ACLsCreate = resolve => { require.ensure(['../views/acls/create.vue'], ()=>{ resolve(require('../views/acls/create.vue')); }); };


// // Content Info
const ContentIndex = resolve => { require.ensure(['../views/content/index.vue'], ()=>{ resolve(require('../views/content/index.vue')); }); };
const ContentEdit = resolve => { require.ensure(['../views/content/edit.vue'], ()=>{ resolve(require('../views/content/edit.vue')); }); };
const ContentCreate = resolve => { require.ensure(['../views/content/create.vue'], ()=>{ resolve(require('../views/content/create.vue')); }); };
const ContentShow = resolve => { require.ensure(['../views/content/show.vue'], ()=>{ resolve(require('../views/content/show.vue')); }); };

// // Content type Info
const ContentTypeIndex = resolve => { require.ensure(['../views/content_type/index.vue'], ()=>{ resolve(require('../views/content_type/index.vue')); }); };
const ContentTypeEdit = resolve => { require.ensure(['../views/content_type/edit.vue'], ()=>{ resolve(require('../views/content_type/edit.vue')); }); };
const ContentTypeCreate = resolve => { require.ensure(['../views/content_type/create.vue'], ()=>{ resolve(require('../views/content_type/create.vue')); }); };
const ContentTypeShow = resolve => { require.ensure(['../views/content_type/show.vue'], ()=>{ resolve(require('../views/content_type/show.vue')); }); };


// // Tag Info
const TagIndex = resolve => { require.ensure(['../views/tag/index.vue'], ()=>{ resolve(require('../views/tag/index.vue')); }); };
const TagEdit = resolve => { require.ensure(['../views/tag/edit.vue'], ()=>{ resolve(require('../views/tag/edit.vue')); }); };
const TagCreate = resolve => { require.ensure(['../views/tag/create.vue'], ()=>{ resolve(require('../views/tag/create.vue')); }); };
const TagShow = resolve => { require.ensure(['../views/tag/show.vue'], ()=>{ resolve(require('../views/tag/show.vue')); }); };

// // Tag Info
const TokenIndex = resolve => { require.ensure(['../views/token/index.vue'], ()=>{ resolve(require('../views/token/index.vue')); }); };
const TokenCreate = resolve => { require.ensure(['../views/token/create.vue'], ()=>{ resolve(require('../views/token/create.vue')); }); };

//vue middleware
import createPermision from './middlewares/createPermision'
import readPermision from './middlewares/readPermision'
import updatePermision from './middlewares/updatePermision'
import admin from './middlewares/admin'


export const routes = [
    {
        path : '',
        name: 'home',
        components:{
            default: Dashboard
        },
        meta: {
            requiresAuth: true,
            middleware:[ [readPermision,'Dashboard'] ]
          }
    },
    {   path : '/dashboard',
        name:'dashboard',
        components:{
            default: Dashboard
        },
        meta: {
            requiresAuth: true,
            middleware:[ [readPermision,'Dashboard'] ]
          }
    },

    {
        path : '',
        name: 'auth',
        component: { render (c) { return c('router-view') }},
        children:[
            {
                path: '/auth/login',
                component: Login,
                name: 'login',
                meta: {
                    default: false,
                    title: 'Login'
                }
            },
            {
                path: '/auth/register',
                component: Register,
                name: 'Register'
            },
            {
                path: '/Page404',
                component: Page404,
                name: 'Page404'
            },
            {
                path: '/Page500',
                component: Page500,
                name: 'Page500'
            },
            {
                path: '/permission-denied',
                component: PermissionDeny,
                name: 'permissiondenied'
            },
            {
                path: '/auth/forget-password',
                component: ForgetPassword,
                name: 'ForgetPassword'
            },
            {
                path: '/password/reset/',
                component: NewPassword,
                name: 'password-reset'
            },


        ]
    },


    {
        path:'/securities',
        name:'securities',
        component: {render(c) {return c('router-view')}},
        children:[
            {
                path:'/securities/user',
                name:'user-index',
                component:UserIndex,
                meta: {
                    requiresAuth: true,
                    middleware:[  [readPermision,'Users'] ]
                  }
            },
            {
                path:'/securities/user/edit/:id',
                name:'user-edit',
                component:UserEdit,
                meta: {
                    requiresAuth: true,
                    middleware:[ [updatePermision,'Users']]
                  }
            },
            {
                path:'/securities/user/create/',
                name:'user-create',
                component:UserCreate,
                meta: {
                    requiresAuth: true,
                    middleware:[ [createPermision,'Users'] ],

                  }
            },
            {
                path:'/securities/user/change-pasword/:id',
                name:'user-change-password',
                component:UserChangePassword,
                meta: {
                    requiresAuth: true,
                    middleware:[  [updatePermision,'Users'] ],

                  }
            },




            {
                path:'/securities/role/index',
                name:'role-index',
                component:RoleIndex,
                meta: {
                    requiresAuth: true,
                    middleware:[ [readPermision,'Roles']  ]

                  }
            },
            {
                path:'/securities/role/edit/:id',
                name:'role-edit',
                component:RoleEdit,
                meta: {
                    requiresAuth: true,
                    middleware:[ [updatePermision,'Roles'] ]

                  }
            },
            {
                path:'/securities/role/create/',
                name:'role-create',
                component:RoleCreate,
                meta: {
                    requiresAuth: true,
                    middleware:[ [createPermision,'Roles'] ],

                  }
            },
            {
                path:'/securities/role/permissions/:role_id',
                name:'role-permission',
                component:RolePermision,
                meta: {
                    requiresAuth: true,
                    middleware:[  [updatePermision,'Roles'] ],

                  }
            },



            {
                path:'/securities/acls/index',
                name:'acls-index',
                component:ACLsIndex,
                meta: {
                    requiresAuth: true,
                    middleware: [ [readPermision,'ACLs']]
                  }
            },
            {
                path:'/securities/acls/edit/:id',
                name:'acls-edit',
                component:ACLsEdit,
                meta: {
                    requiresAuth: true,
                    middleware:[  [updatePermision,'ACLs'] ],

                  }
            },
            {
                path:'/securities/acls/create/',
                name:'acls-create',
                component:ACLsCreate,
                meta: {
                    requiresAuth: true,
                    middleware:[ [createPermision,'ACLs'] ],

                  }
            },



            ///route for token 
            {
                path:'/securities/token/index',
                name:'token-index',
                component:TokenIndex,
                meta: {
                    requiresAuth: true,
                    middleware:[ [admin] ],

                  }
            },
            {
                path:'/securities/token/create/',
                name:'token-create',
                component:TokenCreate,
                meta: {
                    requiresAuth: true,
                    middleware:[ [admin] ],

                  }
            },
        ]

    },

    {
        path:'/content',
        name:'content',
        component: {render(c) {return c('router-view')}},
        children:[
            {
                path:'/content/index',
                name:'content-index',
                component:ContentIndex,
                meta: {
                    requiresAuth: true,
                    middleware: [[readPermision,'Contents']]
                  }
            },
            {
                path:'/content/edit/:id',
                name:'content-edit',
                component:ContentEdit,
                meta: {
                    requiresAuth: true,
                    middleware:[  [updatePermision,'Contents'] ],

                  }
            },
            {
                path:'/content/create/',
                name:'content-create',
                component:ContentCreate,
                meta: {
                    requiresAuth: true,
                    middleware:[  [createPermision,'Contents'] ],

                  }
            },
            {
                path:'/content/show/:id',
                name:'content-show',
                component:ContentShow,
                meta: {
                    requiresAuth: true,
                    middleware:[  [readPermision,'Contents'] ],

                  }
            },

            //content type
            {
                path:'/content/content-type/index',
                name:'content-type-index',
                component:ContentTypeIndex,
                meta: {
                    requiresAuth: true,
                    middleware: [[readPermision,'ContentTypes'] ]
                  }
            },
            {
                path:'/content/content-type/edit/:id',
                name:'content-type-edit',
                component:ContentTypeEdit,
                meta: {
                    requiresAuth: true,
                    middleware:[  [updatePermision,'ContentTypes'] ],

                  }
            },
            {
                path:'/content/content-type/create/',
                name:'content-type-create',
                component:ContentTypeCreate,
                meta: {
                    requiresAuth: true,
                    middleware:[  [createPermision,'ContentTypes'] ],

                  }
            },
            {
                path:'/content/content-type/show/:id',
                name:'content-type-show',
                component:ContentTypeShow,
                meta: {
                    requiresAuth: true,
                    middleware:[  [readPermision,'ContentTypes'] ],

                  }
            },

            //tag
            {
                path:'/content/tag/index',
                name:'tag-index',
                component:TagIndex,
                meta: {
                    requiresAuth: true,
                    middleware: [[readPermision,'Tags'] ]
                  }
            },
            {
                path:'/content/tag/edit/:id',
                name:'tag-edit',
                component:TagEdit,
                meta: {
                    requiresAuth: true,
                    middleware:[  [updatePermision,'Tags'] ],

                  }
            },
            {
                path:'/content/tag/create/',
                name:'tag-create',
                component:TagCreate,
                meta: {
                    requiresAuth: true,
                    middleware:[  [createPermision,'Tags'] ],

                  }
            },
            {
                path:'/content/tag/show/:id',
                name:'tag-show',
                component:TagShow,
                meta: {
                    requiresAuth: true,
                    middleware:[  [createPermision,'Tags'] ],

                  }
            },
           
        ]

    },
    //Redirect to Home
    { path: '/redirect-me', redirect: { name: 'home' } },

    // 404 redirect to home
    { path: '*', redirect: { name: 'Page404', component: Page404 }  },
];