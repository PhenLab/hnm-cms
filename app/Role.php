<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;

class Role extends Model
{
    //
    protected $table = 'roles';
    protected $fillable=[
        'name',
        'code',
        'desc',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'

    ];
    
    public static function getRoleIds($role_names){
        $role_ids = [];
        if(is_array($role_names)){
            $count = count($role_names);
            foreach($role_names as $key=>$role_name){
                array_push($role_ids, Role::getRoleIds($role_name));
                if($count == $key+1){
                    return $role_ids;
                }
            }
        }else{
            return Role::getRole($role_names)->id;
        }
    }
    public static function getRole($name){

        \Log::debug('role_name'. $name);
        $roles = config('roles');
        $code = $roles[$name];
        \Log::debug('role_code'. $code);

        $role = Role::where('code', $code)->first();
        if ( $role ) { return $role; }
        else { return abort('501', 'Role "'.$code.'" not found. Please contact administrator to report the issue.'); }
    }

    public function acls(){
        return $this->belongsToMany('App\Acls', 'role_acls', 'role_id', 'acl_id');
    }
    public function user(){
        return $this->belongsTo('\App\User','created_by');
    }
    
}
