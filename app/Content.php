<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    //
    protected $fillable = [
        'title',
        'body',
        'keyword',
        'tag_id',
        'content_type_id',
        'created_by',
        'updated_at'

    ];
    public function tags(){
        return $this->belongsToMany('\App\Tag', 'content_tags', 'content_id', 'tag_id')->withTimestamps();
    }

    public function content_type(){
        return $this->belongsTo('\App\ContentType');
    }
    public function content_attachments(){
        return $this->hasMany('\App\ContentAttachment');
    }
    public function user(){
        return $this->belongsTo('\App\User','created_by');
    }
}
