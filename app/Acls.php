<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acls extends Model
{
    //
    protected $table = 'acls';
    protected $fillable = [
        'name',
        'desc',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'

    ];
    public function user(){
        return $this->belongsTo('\App\User','created_by');
    }
  
}
