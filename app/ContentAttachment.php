<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentAttachment extends Model
{
    //
    protected $fillable = [
        'content_id',
        'url',
        'created_by',
    ];
}
