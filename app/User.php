<?php

namespace App;

use App\Notifications\MailResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Role;
use Illuminate\Support\Facades\DB;
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = [
        'name',
        'email',
        'photo',
        'status',
        'created_by',
        'updated_by',
        'password'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function roles(){
        return $this->belongsToMany('\App\Role', 'user_roles', 'user_id', 'role_id')->withTimestamps();
    }

    public function hasRole($roles){
        $all_roles = config('roles');
        $input_roles = explode('|', $roles);

        // current user role
        $user_roles = $this->roles()->pluck('code')->toArray();


        foreach($input_roles as $role){
            $role_code = $all_roles[$role];
            if(in_array($role_code, $user_roles)){
                return true;
            }
        }
        return false;
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordNotification($token));
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    
    public function getJWTCustomClaims()
    {
        return [];
    }


    public function hasPerm($permission){
        $exploded = explode('-',$permission);
        $action = $exploded[0];
        $permission_name = $exploded[1];

        $roles = $this->roles()->pluck('roles.id')->all();

        $q = DB::table('role_acls')->join('acls', 'acls.id', 'role_acls.acl_id')
                ->select('role_acls.create', 'role_acls.update', 'role_acls.read', 'role_acls.delete')
                ->whereIn('role_acls.role_id', $roles)
                ->where('acls.name', $permission_name);
        
        switch($action){
            case 'read' :
                $q = $q->where('read', 1);
                break;
            case 'create' :
                $q = $q->where('create', 1);
                break;
            case 'update' :
                $q = $q->where('update', 1);
                break;
            case 'delete' :
                $q = $q->where('delete', 1);
                break;
            default :
                return false;
            break;
        }

        return $q->count() > 0;
    }
    
}
