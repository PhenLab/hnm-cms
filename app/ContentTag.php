<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentTag extends Model
{
    //
    protected $fillable = [
        'content_id',
        'tag_id',
        'created_by',

    ];
}
