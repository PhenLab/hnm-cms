<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentType extends Model
{
    //
    protected $fillable = [
        'title',
        'desc',
        'attachment',
        'created_by',
        'updated_by'

    ];

    public function user(){
        return $this->belongsTo('\App\User','created_by');
    }
}
