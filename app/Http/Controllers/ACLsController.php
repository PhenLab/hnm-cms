<?php

namespace App\Http\Controllers;

use App\Acls;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ACLsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        # code...

        $acls = Acls::orderBy('name');

        $keyword = $request->keyword;
        if($keyword)
            $acls = $acls->where('name','like','%'.$keyword.'%');
                
        $acls->with('user');    
        $acls = $acls->paginate($this->pagination);

        return response()->json([
            'status'=>'success',
            'acls'=>$acls,

        ],200);


    }
    public function store(Request $request)
    {

        
        $v = Validator::make($request->all(), [
            'name' => 'required|string|max:255|unique:acls'
        ]);

        if ($v->fails()) {
            
            return response()->json([
                'status'=>'error',
                'errors'=>$v->errors()
            ],422);
        }
        
        $data = [
            'name'=>$request->name,
            'desc'=>$request->desc,
            'created_by'=> Auth::user()->id,
            'updated_by'=> Auth::user()->id,

        ];
        
        $acls = Acls::create($data);

        return response()->json([
            'status'=>'success',
            'acls'=> $acls,
        ],200);
    }

    public function update(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required|string|max:255|unique:acls,name,'.$request->id,        

        ]);

        if ($v->fails()) {
            
            return response()->json([
                'status'=>'error',
                'errors'=>$v->errors()
            ],422);
        }
        $data = [
            'name'=>$request->name,
            'desc'=>$request->desc,
            'updated_by'=> Auth::user()->id,
            'updated_at'=> Carbon::now(),

        ];
        $acls = Acls::find($request->id);
        $acls->update($data);

        return response()->json([
            'status'=>'success',
            'acls'=> $acls,
        ],200);
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $acls = Acls::findOrFail($request->id);
        DB::table('role_acls')->where('acl_id', $id)->delete();

        $acls->delete();
        return response()->json([
            'status'=>'success',
            'acl'=>$acls,
        ],200);
    }
    public function show(Request $request)
    {
        # code...
        $acls = Acls::find($request->id);

        return response()->json([
            'status'=>'success',
            'acls'=> $acls,
        ],200);
    }

   
}
