<?php

namespace App\Http\Controllers;

use App\Content;
use App\ContentType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

class ContentTypeController extends Controller
{
    //
    public $defaultContentType = ['image','video','file'];
    public function index(Request $request)
    {
        # code...

        // $q = DB::table('content_types');
        $q = ContentType::where('id','!=', 0);

        $keyword = $request->keyword;
        if($keyword){
            $q = $q->where(function($q) use($keyword){
                $q->where('title','like','%'.$keyword.'%')
                    ->orWhere('desc','like','%'.$keyword.'%');
            });
        }

        $is_paginate = $request->is_paginate;
        if($is_paginate == 'false'){
            $q->orderBy('title','asc');
            $q = $q->get();
        }else{
            $q->with('user');
            $q->orderBy('created_at','desc');
            $q = $q->paginate($this->pagination);
        }      
        
            
        return response()->json([
            'status'=>'success',
            'datas'=>$q,

        ],200);


    }
    public function store(Request $request)
    {

        
        $v = Validator::make($request->all(), [
            'title' => 'required|string|max:255|unique:content_types'
        ]);

        if ($v->fails()) {
            
            return response()->json([
                'status'=>'error',
                'errors'=>$v->errors()
            ],422);
        }
        
        $data = [
            'title'=>$request->title,
            'desc'=>$request->desc,
            'attachment'=>$request->attachment=='true'?1:0,
            'created_by'=> Auth::user()->id,
            'updated_by'=> Auth::user()->id,

        ];
        
        $obj = ContentType::create($data);

        return response()->json([
            'status'=>'success',
            'data'=> $obj,
        ],200);
    }

    public function update(Request $request)
    {
        
        $v = Validator::make($request->all(), [
            'title' => 'required|string|max:255|unique:content_types,title,'.$request->id,        

        ]);

        if ($v->fails()) {
            
            return response()->json([
                'status'=>'error',
                'errors'=>$v->errors()
            ],422);
        }
        $data = [
            'title'=>$request->title,
            'desc'=>$request->desc,
            'attachment'=>$request->attachment =='true'?1:0,
            'updated_by'=> Auth::user()->id,
            'updated_at'=> Carbon::now(),

        ];
        $obj = ContentType::find($request->id);

        if( in_array($obj->title, $this->defaultContentType)){
            return response()->json([
                'status'=>'error',
                'msg'=>'You can not update this content type',

            ],550);
        }

        $obj->update($data);

        return response()->json([
            'status'=>'success',
            'data'=> $obj,
        ],200);
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $obj = ContentType::findOrFail($request->id);

        if( in_array($obj->title, $this->defaultContentType)){
            return response()->json([
                'status'=>'error',
                'msg'=>'You can not delete this content type',

            ],550);
        }
        $is_used = Content::whereHas('content_type', function($q) use($id){
            $q->where('content_type_id', $id);
        })->first();
    
        if($is_used){
            return response()->json([
                'status'=>'error',
                'msg'=>'This content type already assigned to contents',

            ],403);
        }
        $obj->delete();
        return response()->json([
            'status'=>'success',
        ],200);
    }
    public function show(Request $request)
    {
        # code...
        $obj = ContentType::find($request->id);
        $obj->user = $obj->user;
        
        return response()->json([
            'status'=>'success',
            'data'=> $obj,
        ],200);
    }

    public function getAllContentTypes()
    {
        # code...
        $obj = ContentType::orderBy('title','asc')->get();
        return response()->json([
            'status'=>'success',
            'datas'=> $obj,
        ],200);
    }
}
