<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $pathFileStore = 'public/files';
    public $realPathFileStore = '/storage/files/';
    public $pathImageStore = 'public/images';
    public $realPathImageStore = '/storage/images/';
    public $pathVideoStore = 'public/videos';
    public $realPathVideoStore = '/storage/videos/';

    public $acls = [
        'Dashboard',
        'Contents',
        'ContentTypes',
        'Tags',
        'Users',
        'Roles',
        'ACLs'
    ];

    public $pagination = 20 ;

}
