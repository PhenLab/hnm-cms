<?php

namespace App\Http\Controllers;

use App\Content;
use App\ContentAttachment;
use App\ContentTag;
use App\ContentType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use App\Http\Controllers\ContentTypeController;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;

class ContentController extends Controller
{
    //
    public function index(Request $request)
    {
        
        $q = Content::orderBy('updated_at','desc');

        $creator_id = $request->creator_id;
        $keyword = $request->keyword;
        $content_type = $request->content_type;
        $tag_id = $request->tag_id;
        $searchText = $request->searchText;
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        if($searchText){
            $q = $q->where(function($q) use($searchText)
            {
                $q->where('title','like','%'.$searchText.'%')
                    ->orWhere('body','like','%'.$searchText.'%');
            });
        }
        if(isset($keyword)){
            $q->where('keyword','like','%'.$keyword.'%');
        }
        if(isset($tag_id) && sizeof($tag_id) > 0){
            $q->whereHas('tags',function($q) use($tag_id)
            {
                $q->whereIn('tag_id',$tag_id);
            });
        }

        if ( isset($creator_id)) {
            $q->where('created_by',$creator_id);
        }
        if ($content_type) {
            $q->where('content_type_id',$content_type);
        }

        //filter by date range 
        if( isset($startDate) && isset($endDate) ){
            $start = Carbon::createFromFormat('d/m/Y', $startDate)->startOfDay();
            $end = Carbon::createFromFormat('d/m/Y', $endDate);
            
            $q = $q->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end);
        }
        
                
        $q->with('content_type'); 
        $q->with('tags');  
        $q->with('user');  

        $is_paginate = $request->is_paginate;
        if($is_paginate == 'false'){
            $q = $q->get();
        }else{
            $q = $q->paginate($this->pagination);
        }    

        return response()->json([
            'status'=>'success',
            'datas'=>$q,

        ],200);


    }
    public function store(Request $request)
    {    
        $type_name = $request->type_name;
        $content_type_id = $request->type_id;
        $obj = new ContentTypeController();
        $image = $obj->defaultContentType[0];
        $video = $obj->defaultContentType[1];
        $file = $obj->defaultContentType[2];
        $content_type = ContentType::find($content_type_id);

        $img_vid_file_type = false ;
        if ($type_name == $image || $type_name == $video || $type_name == $file) {
            $img_vid_file_type = true;
        }

        $v = Validator::make($request->all(), [
            // 'title' => 'required|string|max:255|unique:contents',
            'title' => 'required|string|max:255',
            'type_id' => 'required',
            'images' => Rule::requiredIf($type_name == $image),
            'videos' => Rule::requiredIf($type_name == $video),
            'files' => Rule::requiredIf( ($type_name == $file) || (isset($content_type_id) &&  !$img_vid_file_type && $content_type->attachment == 1) ),
            // 'files' => Rule::requiredIf(isset($content_type_id) &&  !$img_vid_file_type && $content_type->attachment == 1),

        ],[
            'type_id.required'=>'The type field is required'
        ]);
        if ($v->fails()) {
            return response()->json([
                'status'=>'error',
                'errors'=>$v->errors()
            ],422);
        }
        $data = [
            'title'=>$request->title,
            'keyword'=>$request->keyword,
            'body'=>$request->body,
            'content_type_id'=>$request->type_id,
            'created_by'=> Auth::user()->id,

        ];
        //save data to content
        $obj = Content::create($data);

        $paths = [];
        if ($type_name == $image) {
            $images = $request->file('images');
            foreach ($images as $key => $value) {
                $paths[$key] = $value->store($this->pathImageStore);
            }
        }
        else if ($type_name == $video) {
            $videos = $request->file('videos');
            foreach ($videos as $key => $value) {
                $paths[$key] = $value->store($this->pathVideoStore);
            }
        }
        else if ($type_name == $file || $content_type->attachment == 1) {
            $files = $request->file('files');
            foreach ($files as $key => $value) {
                $paths[$key] = $value->store($this->pathFileStore);
            }
        }
        //save data to content attachments
        foreach ($paths as $key => $value) {
            $path = Storage::url($value);
            $field = [
                'content_id'=>$obj->id,
                'url'=>$path,
                'created_by'=>Auth::user()->id,
            ];

            $result = ContentAttachment::create($field);
        }

        //save data to content tage
        if (isset($request->tag_id)) {
            
            foreach ($request->tag_id as $key => $id) {
                ContentTag::create([
                    'content_id'=>$obj->id,
                    'tag_id'=>$id,
                    'created_by'=>Auth::user()->id,

                ]);
            }
            
        }
    

        return response()->json([
            'status'=>'success',
            'data'=> $obj,
        ],200);
    }

    public function update(Request $request)
    {

        $id = $request->id;
        $type_name = $request->type_name;
        $obj = new ContentTypeController();
        $image = $obj->defaultContentType[0];
        $video = $obj->defaultContentType[1];
        $file = $obj->defaultContentType[2];

        $notDeleteVideos = $request->notDeletevideos;
        $notDeleteFiles = $request->notDeletefiles;
        $notDeleteImages = $request->notDeleteimages;
        $content_type_id = $request->type_id;
        $content_type = ContentType::find($content_type_id);

        $img_vid_file_type = false ;
        if ($type_name == $image || $type_name == $video || $type_name == $file) {
            $img_vid_file_type = true;
        }

        $v = Validator::make($request->all(), [
            // 'title' => 'required|string|max:255|unique:contents,title,'.$id,
            'title' => 'required|string|max:255',
            'type_id' => 'required',
            'images' => Rule::requiredIf($type_name == $image && !isset($notDeleteImages)),
            'videos' => Rule::requiredIf($type_name == $video && !isset($notDeleteVideos)),
            'files' => Rule::requiredIf( ($type_name == $file && !isset($notDeleteFiles)) || 
                                        (isset($content_type_id) &&  !$img_vid_file_type && $content_type->attachment == 1 && !isset($notDeleteFiles)) ),
            // 'files' => Rule::requiredIf( isset($content_type_id) &&  !$img_vid_file_type && $content_type->attachment == 1 && !isset($notDeleteFiles)),

        ],[
            'type_id.required'=>'The type field is required'
        ]);
        if ($v->fails()) {
            return response()->json([
                'status'=>'error',
                'errors'=>$v->errors()
            ],422);
        }
        $data = [
            'title'=>$request->title,
            'keyword'=>$request->keyword,
            'body'=>$request->body,
            'content_type_id'=>$request->type_id,
            'updated_at'=> Carbon::now(),

        ];

        //save data to content
        $obj = Content::find($id);
        $obj->update($data);

        $paths = [];
        $cont_att = ContentAttachment::where('content_id',$id)->get();

        $notDeleteMedia = null;
        $completedurl = '';
        if ($type_name == $image) {
            foreach ($cont_att as $key => $value) {
                $urls = explode('/',$value->url);
                if ($notDeleteImages == null || !in_array($urls[sizeof($urls) - 1], $notDeleteImages)) {
                    File::delete($value->url);
                }
            }
            $images = $request->file('images');
            if ($images != null) {
                # code...
                foreach ($images as $key => $value) {
                    $paths[$key] = $value->store($this->pathImageStore);
                }
            }
            $completedurl = $this->realPathImageStore;
            $notDeleteMedia = $notDeleteImages;
        }
        else if ($type_name == $video) {            
            foreach ($cont_att as $key => $value) {
                $urls = explode('/',$value->url);
                if ($notDeleteVideos == null || !in_array($urls[sizeof($urls) - 1], $notDeleteVideos)) {
                    File::delete($value->url);
                }
            }
            $videos = $request->file('videos');
            if ($videos != null) {
                foreach ($videos as $key => $value) {
                    $paths[$key] = $value->store($this->pathVideoStore);
                }
            }
            $completedurl = $this->realPathVideoStore;
            $notDeleteMedia = $notDeleteVideos;
        }
        else if ($type_name == $file || $content_type->attachment == 1) {
            foreach ($cont_att as $key => $value) {
                $urls = explode('/',$value->url);
                if ( $notDeleteFiles == null || !in_array($urls[sizeof($urls) - 1], $notDeleteFiles)) {
                    # code...     
                    File::delete($value->url);
                }
            }
            $files = $request->file('files');
            if ($files != null) {
                foreach ($files as $key => $value) {
                    $paths[$key] = $value->store($this->pathFileStore);
                }
            }
            $completedurl = $this->realPathFileStore;
            $notDeleteMedia = $notDeleteFiles;
        }
        //save data to content attachments
        ContentAttachment::where('content_id',$id)->delete();
        
        foreach ($paths as $key => $value) {
            $path = Storage::url($value);
            $field = [
                'content_id'=>$obj->id,
                'url'=>$path,
                'created_by'=>Auth::user()->id,
            ];
            ContentAttachment::create($field);
        }
        
        if ($notDeleteMedia != null) {
            foreach ($notDeleteMedia as $key => $value) {
                $field = [
                    'content_id'=>$obj->id,
                    'url'=>$completedurl . $value,
                    'created_by'=>Auth::user()->id,
                ];
                ContentAttachment::create($field);
            }
        }
        

        //save data to content tage
        ContentTag::where('content_id',$id)->delete();
        if (isset($request->tag_id)) {
            foreach ($request->tag_id as $key => $tag_id) {
                ContentTag::create([
                    'content_id'=>$obj->id,
                    'tag_id'=>$tag_id,
                    'created_by'=>Auth::user()->id,

                ]);
            }
            
        }
    
        return response()->json([
            'status'=>'success',
            'data'=> $obj,
        ],200);
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $obj = Content::findOrFail($id);

        $obj->delete();
        //delete Content attachment
        $cont_att = ContentAttachment::where('content_id',$id)->get();
        foreach ($cont_att as $key => $value) {
            File::delete($value->url);  
        }
        ContentAttachment::where('content_id',$id)->delete();
        ContentTag::where('content_id',$id)->delete();
        return response()->json([
            'status'=>'success',
        ],200);
    }
    public function show(Request $request)
    {
        # code...
        $obj = Content::find($request->id);

        $tags = $obj->tags()->get();
        $content_type = $obj->content_type()->get();
        $content_attachments = $obj->content_attachments()->get();
        $user = $obj->user()->first();

        return response()->json([
            'status'=>'success',
            'data'=> $obj,
            'tags'=> $tags,
            'content_type'=> $content_type,
            'content_attachments'=> $content_attachments,
            'user'=> $user,

        ],200);
    }
    public function getTotalContents()
    {
        # code...
        $contents = Content::all();

        return response()->json([
            'status'=>'success',
            'datas'=>$contents,

        ],200);

    }
}
