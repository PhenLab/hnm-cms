<?php

namespace App\Http\Controllers;

use App\ApiSession;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class TokenController extends Controller
{
    //
    public function index(Request $request){
        $keyword = $request->keyword;
        
        $tokens = ApiSession::orderBy('desc');
        
        if($keyword){
            $tokens = $tokens->where('desc', 'LIKE', "%$keyword%");
        }
        $tokens->with('user');
        $tokens = $tokens->paginate($this->pagination);

        return response()->json([
            'status'=>'success',
            'datas'=>$tokens,

        ],200);    
    }

    public function store(Request $request){
        
        $v = Validator::make($request->all(), [
            'desc' => 'required'
        ],[
            'required'=>'The description field is required.'
        ]);

        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }
        $user =  Auth::user();
        
        $token = ApiSession::create([
            'user_id' => $user->id,
            'desc'=> $request->desc,
            'api_token' => auth('api')->tokenById($user->id),
            'type' => 'Mobile',
        ]);

        return response()->json([
            'status'=>'success',
            'data'=> $token,
        ],200);
    }
    
}
