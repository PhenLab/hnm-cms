<?php

namespace App\Http\Controllers;

use App\Setting;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class SettingController extends Controller
{
    //
    
    public function changeAppName(Request $request)
    {
        # code...
        $v = Validator::make($request->all(), [
            'name' => 'required|string',        
        ]);

        if ($v->fails()) {
            return response()->json([
                'status'=>'error',
                'errors'=>$v->errors()
            ],422);
        }
        $setting = Setting::where('type','app_name')->first();
        if ($setting) {
            # code...
            $setting->content =  $request->name;
            $setting->save();
        }
        else{
            $setting = new Setting();
            $setting->type = 'app_name';
            $setting->content = $request->name;
            $setting->save();
        }

        return response()->json(['status'=>'success'],200);

    }
    public function getAppName()
    {

        $setting = Setting::where('type','app_name')->first();
        if( !$setting ){
            $setting = new Setting();
            $setting->type = 'app_name';
            $setting->content = 'HNM CMS';
            $setting->save();
        }
        
        return response()->json( [ 'status'=>'success', 'name'=>$setting ] ,200);
    }
}
