<?php

namespace App\Http\Controllers\API;

use App\Content;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class ContentController extends Controller
{
    //
    public function searchFilter(Request $request)
    {
        $v = Validator::make($request->all(), [
            
            'tag_ids' => 'nullable|array',
            'startDate' => 'nullable|date_format:d/m/Y',
            'endDate' => 'nullable|date_format:d/m/Y',

        ]);
        if ($v->fails()) {
            # code...
            return ['status' => 'error', 'code' => 422, 'msg' => implode(", " ,$v->errors()->all())];

        }
        $q = Content::orderBy('updated_at','desc');

        $searchText = $request->search;
        $keyword = $request->keyword;
        $tag_id = $request->tag_ids;
        $content_type = $request->content_type;
        $creator_id = $request->created_by;
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        if($searchText){
            $q = $q->where(function($q) use($searchText)
            {
                $q->where('title','like','%'.$searchText.'%')
                    ->orWhere('body','like','%'.$searchText.'%');
            });
        }
        if(isset($keyword)){
            $q->where('keyword','like','%'.$keyword.'%');
        }
        if(isset($tag_id) && sizeof($tag_id) > 0){
            $q->whereHas('tags',function($q) use($tag_id)
            {
                $q->whereIn('tag_id',$tag_id);
            });
        }

        if ( isset($creator_id)) {
            $q->where('created_by',$creator_id);
        }
        
        if ($content_type) {
            $q->where('content_type_id',$content_type);
        }

        //filter by date range 
        if( isset($startDate) && isset($endDate) ){
            $start = Carbon::createFromFormat('d/m/Y', $startDate)->startOfDay();
            $end = Carbon::createFromFormat('d/m/Y', $endDate);
            
            $q = $q->whereDate('created_at', '>=', $start)->whereDate('created_at', '<=', $end);
        }
        $q = $q->paginate($this->pagination);
        $q = $q->toArray();
        $q = $q['data'];
        $total = count($q);

        return response()->json([
            'status'=>'success',
            'total'=>$total,
            'contents'=>$q
        ]);


    }

}
