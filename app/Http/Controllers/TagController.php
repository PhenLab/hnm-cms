<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller
{
    //
    public function index(Request $request)
    {
        # code...

        $q = Tag::orderBy('tag');

        $keyword = $request->keyword;
        if($keyword){
            $q->where('tag','like','%'.$keyword.'%');
        
        }
                
        $is_paginate = $request->is_paginate;
        if($is_paginate == 'false'){
            $q = $q->get();
        }else{
            $q->with('user');
            $q = $q->paginate($this->pagination);
        }  

        return response()->json([
            'status'=>'success',
            'datas'=>$q,

        ],200);


    }
    public function store(Request $request)
    {
        
        $v = Validator::make($request->all(), [
            'tag' => 'required|string|max:255|unique:tags'
        ]);

        if ($v->fails()) {
            
            return response()->json([
                'status'=>'error',
                'errors'=>$v->errors()
            ],422);
        }
        
        $data = [
            'tag'=>$request->tag,
            'created_by'=> Auth::user()->id,

        ];
        
        $obj = Tag::create($data);

        return response()->json([
            'status'=>'success',
            'data'=> $obj,
        ],200);
    }

    public function update(Request $request)
    {
        $v = Validator::make($request->all(), [
            'tag' => 'required|string|max:255|unique:tags,tag,'.$request->id,        

        ]);

        if ($v->fails()) {
            
            return response()->json([
                'status'=>'error',
                'errors'=>$v->errors()
            ],422);
        }
        $data = [
            'tag'=>$request->tag,
            

        ];
        $obj = Tag::find($request->id);
        $obj->update($data);

        return response()->json([
            'status'=>'success',
            'data'=> $obj,
        ],200);
    }

    public function delete(Request $request)
    {
        $obj = Tag::findOrFail($request->id);

        $obj->delete();
        return response()->json([
            'status'=>'success',
        ],200);
    }
    public function show(Request $request)
    {
        # code...
        $obj = Tag::find($request->id);

        $obj->user = $obj->user;
        return response()->json([
            'status'=>'success',
            'data'=> $obj,
        ],200);
    }
    public function getAllTags()
    {
        # code...
        $obj = Tag::all();
        return response()->json([
            'status'=>'success',
            'datas'=> $obj,
        ],200);
    }
}
