<?php

namespace App\Http\Controllers;

use App\Acls;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use Auth;
use stdClass;

class AuthController extends Controller
{
    //
    public function register(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users',
            'password'  => 'required|min:6|confirmed',
        ]);
        
        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }
        $data =  $request->input();
        $data['password']  = bcrypt($request->password);
        $user = new User;
        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = bcrypt($request->password);
        $user->save();
        return response()->json(['status' => 'success'], 200);
    }
    public function login(Request $request)
    {
        $v = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string',

        ]);
        
        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }

        $credentials = $request->only('email', 'password');
        if ($token = auth()->attempt($credentials)) {

            $user = Auth::user();
            //list of allowed ips
            // $allowed_ips = $this->getAllowedIPs();

            // if( sizeof($allowed_ips) > 0 && !in_array($request->ip(), $allowed_ips) ){
            //     return response()->json(['status' => 'error','msg' => "You don't has permission to acess this web"]);
            // }
            if($user->status == 0){
                $this->guard()->logout();
                return response()->json(['status' => 'error','msg' => 'This account has been blocked']);

            }
  
            return response()->json(['status' => 'success','token'=>$token,'user'=>$user,'rolePerms'=>self::getUserRolePerm($user)], 200);
        }
        return response()->json(['status' => 'error','msg' => "Credentials doesn't match"]);

        // return response()->json(['error' => 'login_error'], 401);
    }
    public function logout()
    {
        $this->guard()->logout();
        return response()->json([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 200);
    }
    public function user(Request $request)
    {
        
        $user = User::find(Auth::user()->id);
        return response()->json([
            'status' => 'success',
            'data' => $user
        ]);
    }
    public function refresh()
    {
        if ($token = $this->guard()->refresh()) {
            return response()
                ->json(['status' => 'successs'], 200)
                ->header('Authorization', $token);
        }
        return response()->json(['error' => 'refresh_token_error'], 401);
    }
    private function guard()
    {
        return Auth::guard();
        
    }
    public static function getUserRolePerm($user)
    {
        # code...
        $roles = new stdClass();
        $acls = Acls::all();

        foreach ($acls as $key => $value) {
            $create = 'create'.$value->name;
            $read = 'read'.$value->name;
            $update = 'update'.$value->name;
            $delete = 'delete'.$value->name;

            $roles->$create = $user->hasPerm('create-'.$value->name);
            $roles->$read = $user->hasPerm('read-'.$value->name);
            $roles->$update = $user->hasPerm('update-'.$value->name);
            $roles->$delete = $user->hasPerm('delete-'.$value->name);
        }
        if ( Auth::user()->hasRole('Administrator') ) {
            $roles->hasRoleAdmin = true;
        }
    
        return $roles;
    }
    
    public function getAllowedIPs()
    {
        # code...
        try {
            //code...
            $allowed_ips =  config('app.allowed_ips');
            $allowed_ips = $allowed_ips ? explode(',',$allowed_ips) : [];

        } catch (\Throwable $th) {
            //throw $th;
            \Log::debug($th);
            $allowed_ips = [];
        }
        return $allowed_ips;
    }
}
