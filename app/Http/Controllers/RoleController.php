<?php

namespace App\Http\Controllers;

use App\Acls;
use Illuminate\Http\Request;
use App\Role;
use App\User;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    //
    public function index(Request $request)
    {
        # code...

        $roles = Role::orderBy('name');

        $keyword = $request->keyword;
        if($keyword)
            $roles = $roles->where(function ($q) use($keyword)
            {
                $q->where('name','like','%'.$keyword.'%')
                ->orWhere('code','like','%'.$keyword.'%');
                
            });
                
        $roles->with('user');    
        $roles = $roles->paginate($this->pagination);

        return response()->json([
            'status'=>'success',
            'roles'=>$roles,

        ],200);


    }
    public function store(Request $request)
    {

        
        $v = Validator::make($request->all(), [
            'name' => 'required|string|max:255|unique:roles',
            'code' => 'required|string|max:50|unique:roles',
        

        ]);

        if ($v->fails()) {
            
            return response()->json([
                'status'=>'error',
                'errors'=>$v->errors()
            ],422);
        }
        
        $data = [
            'name'=>$request->name,
            'code'=>$request->code,
            'desc'=>$request->desc,
            'created_by'=> Auth::user()->id,
            'updated_by'=> Auth::user()->id,

        ];
        
        $role = Role::create($data);

        return response()->json([
            'status'=>'success',
            'role'=> $role,
        ],200);
    }

    public function update(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required|string|max:255|unique:roles,name,'.$request->id,
            'code' => 'required|string|max:50|unique:roles,code,'.$request->id,
        

        ]);

        if ($v->fails()) {
            
            return response()->json([
                'status'=>'error',
                'errors'=>$v->errors()
            ],422);
        }
        $data = [
            'name'=>$request->name,
            'code'=>$request->code,
            'desc'=>$request->desc,
            'updated_by'=> Auth::user()->id,
            'updated_at'=> Carbon::now(),

        ];
        $role = Role::find($request->id);
        $role->update($data);

        return response()->json([
            'status'=>'success',
            'role'=> $role,
        ],200);
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $role = Role::find($id);
        $is_used = User::whereHas('roles', function($q) use($id){
            $q->where('role_id', $id);
        })->first();
    
        if($is_used){
            return response()->json([
                'status'=>'error',
                'msg'=>'This role already assigned to users',

            ],403);
        }
        $role->delete();
        return response()->json([
            'status'=>'success',
        ],200);
    }
    public function show(Request $request)
    {
        # code...
        $role = Role::find($request->id);

        return response()->json([
            'status'=>'success',
            'role'=> $role,
        ],200);
    }
    public function permission(Request $request)
    {
        # code...
        $id =  $request->role_id;

        $perms = DB::table('acls')
            ->leftJoin('role_acls as rp', function($join) use($id){
            $join->on('rp.acl_id', 'acls.id')->where('rp.role_id', $id);
        })->leftJoin('roles', function($join) use($id){
            $join->on('roles.id', 'rp.role_id')->where('roles.id', $id);
        });

        $perms = $perms->select('rp.id as id', 'acls.id as acl_id', DB::raw($id. ' as role_id'), 'rp.read', 'rp.update', 'rp.create', 'rp.delete', 'acls.name')->get();

        $role =  Role::find($id);
        return response()->json([
            'status'=>'success',
            'perms'=> $perms,
            'role'=>$role,
        ],200);
    }

    public function updatePermission(Request $request)
    {
        $id = $request->role_id;
        $status = $request->status == 'true'?1:0;
        $this->validate($request, [
            'acl_id' => 'required', 
            'type' => 'required|in:read,create,update,delete',
            'status' => 'required'
        ]);
        $role = Role::findOrFail($id);

        $user = Auth::user();
        $obj  = new AuthController();
        if($role->code == config('role.Administrator') && !Auth::user()->hasRole('Administrator')){
            return response()->json([
                'status'=>'error',
            ],403); 
        }


        $permission = Acls::findOrFail($request->acl_id);
        $existing = DB::table('role_acls')->where('role_id', $id)->where('acl_id', $request->acl_id)->first();

        if($existing){
            DB::table('role_acls')->where('role_id', $id)->where('acl_id', $request->acl_id)->update([
                $request->type => $status,
                'updated_at' => Carbon::now(),
                'updated_by' => Auth::user()->id

            ]);
        }else{

            $data = [
                'role_id' => $id,
                'acl_id' => $request->acl_id,
                'read' => false,
                'create' => false,
                'update' => false,
                'delete' => false,
                'created_by' => Auth::user()->id,
                'created_at' => Carbon::now(),
                'updated_by' => Auth::user()->id,
                'updated_at' => Carbon::now()
            ];

            if($data[$request->type] === false){
                $data[$request->type] = $status;
            }

            DB::table('role_acls')->insert($data);
        }

        return response()->json([
            'status'=>'success',
            'roles'=>$obj->getUserRolePerm($user)
        ],200);

    }

    public function getAllACLs()
    {
        
        return response()->json([
            'acls'=> $this->acls,
        ],200);
    }
    public function getUserPermission()
    {
        $user = Auth::user();
        $permissions = AuthController::getUserRolePerm($user);

        return response()->json([
            'status'=>'success',
            'permissions'=>$permissions
        ],200);

    }
}
