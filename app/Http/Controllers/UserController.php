<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Request as FacadesRequest;
use Auth;

class UserController extends Controller
{
    public function __construct() {
        $this->hiddenRoles = [ config('roles.Administrator')];

    }

    //
    public function getAllusers(Request $request)
    {
        
        $roles = $request->role;
        $search = $request->keyword;

        $users = User::where('id','!=',0);

        if (! Auth::user()->hasRole('Administrator')) {
            $users = $users->whereDoesntHave('roles', function($q){
                $q->whereIn('roles.code', $this->hiddenRoles);
            });
        }
       
        if($roles){
            $users->whereHas('roles',function($q) use($roles)
            {
                $q->where('role_id',$roles);
            });
        }        
       
        if($search){
            $users->where(function($r) use ($search)
            {
              $r->where('name','like','%'.$search.'%')
                         ->orWhere('email','like','%'.$search.'%');
            });
        }

        $users->with('roles');

        $is_paginate = $request->is_paginate;
        if($is_paginate == 'false'){
            $users->orderBy('name','asc');
            $users = $users->get();
        }else{
            $users->orderBy('updated_at','desc');
            $users = $users->paginate($this->pagination);

        }
        
        return response()->json(
            [
                'status' => 'success',
                'users' => $users
            ], 200);
    }
    public function getUser(Request $request)
    {
        $user = User::find($request->id);
        
        return response()->json(
            [
                'status' => 'success',
                'user' => $user
            ], 200);
    }
    public function show(Request $request, $id)
    {
        $user = User::find($id);
        return response()->json(
            [
                'status' => 'success',
                'user' => $user->toArray()
            ], 200);
    }

    public function store(Request $request)
    {

       
        $v = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users',
            'password'  => 'required|min:6|confirmed',
            'roles'  => 'required',

        ]);

        if ($v->fails()) {
            
            return response()->json([
                'status'=>'error',
                'errors'=>$v->errors()
            ],422);
        }
        $data = [
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=>bcrypt($request->password),

        ];
        if ($request->hasFile('file')) {
            # code...
            $path = $request->file('file')->store('public/photo');
            $paths = explode('/',$path,2);
            $path = 'storage/'.$paths[1];
            $data['photo'] = $path;
        }
        $user = User::firstOrCreate($data);
        $user_id = $user->id;
        //assign role

        if ($request->roles) {
            # code...
            $roles = json_decode( $request->roles);

            foreach ($roles as $key => $value) {
                # code...

                DB::table('user_roles')->insert([
                    'user_id'=>$user_id,
                    'role_id'=>$value
                ]);

            }
        }
        return response()->json([
            'status'=>'success',
            'user'=> $user,
        ],200);
    }

    public function getAllRoles(Request $request)
    {
        # code...
        $roles = Role::all();
        return response()->json([
            'status'=>'success',
            'roles'=>$roles,

        ],200);
    }
    public function getUserRoles(Request $request)
    {
        # code...
        $user = User::find($request->id);
        
        $roles = $user->roles()->get();
        return response()->json([
            'status'=>'success',
            'roles'=>$roles,
        ],200);
    }
    public function update(Request $request)
    {     
    
        $v = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users,email,'.$request->id,
            'roles'  => 'required',

        ]);

        if ($v->fails()) {
            
            return response()->json([
                'status'=>'error',
                'errors'=>$v->errors()
            ],422);
        }
        $data = [
            'name'=>$request->name,
            'email'=>$request->email,

        ];
        $user = User::find($request->id);
        if ($request->hasFile('file')) {
            # code...
            if ($user->photo) 
                File::delete($user->photo);

            $path = $request->file('file')->store('public/photo');
            $paths = explode('/',$path,2);
            $path = 'storage/'.$paths[1];
            $data['photo'] = $path;
        }

        $user->update($data);
        $user_id = $request->id;
        //assign role
        DB::table('user_roles')->where('user_id', $request->id)->delete();
        if ($request->roles) {
            # code...
            $roles = json_decode( $request->roles);

            foreach ($roles as $key => $value) {
                # code...
                DB::table('user_roles')->insert([
                    'user_id'=>$user_id,
                    'role_id'=>$value
                ]);

            }
        }
        return response()->json([
            'status'=>'success',
            'user'=> $user,
        ],200);
    }

    public function changePassword(Request $request)
    {

        $v = Validator::make($request->all(), [
            'password'  => 'required|min:6|confirmed',

        ]);

        if ($v->fails())
        {
            return response()->json([
                'status' => 'error',
                'errors' => $v->errors()
            ], 422);
        }
        $data = [
            'password'=>bcrypt($request->password),
        ];
        $user = User::find($request->id);

        $user->update($data);
        
        return response()->json([
            'status'=>'success',
        ],200);
    }

    public function block(Request $request)
    {

        $status = 1;
        $user = User::find($request->id);

        $status = $request->status==1?0:1;

        $user->update(['status'=>$status]);
        
        return response()->json([
            'status'=>'success',
            'user_status'=>$status,

        ],200);
    }

    public function getUsers(Request $request)
    {
        
        $users = User::where('id','!=',0);

        if (! Auth::user()->hasRole('Administrator')) {
            $users = $users->whereDoesntHave('roles', function($q){
                $q->whereIn('roles.code', $this->hiddenRoles);
            });
        }
       
        $users->orderBy('name','asc');
        $users = $users->get();
        
        return response()->json(
            [
                'status' => 'success',
                'users' => $users
            ], 200);
    }
}
