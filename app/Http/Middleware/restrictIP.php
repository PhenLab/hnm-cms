<?php

namespace App\Http\Middleware;

use Closure;
use App\AllowedIPs;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\App;

class restrictIP
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $allowed_ip = AllowedIPs::pluck('ip')->toArray();
        $obj = new AuthController();
        $allowed_ips = $obj->getAllowedIPs();
        $request_ip = $request->ip();
        foreach ($allowed_ips as $key => $value) {
            # code...
            if( strpos($value, '-') ){
                
                $rangs = explode('-',$value);
                $start_ip = $rangs[0];
                $end_ip = $rangs[1];
                $start_ip = trim($start_ip);
                
                $start_host_num = explode('.',$start_ip);
                $ip_head = $start_host_num[0].'.' . $start_host_num[1] .'.'. $start_host_num[2];
                $start_host_num = $start_host_num[ sizeof($start_host_num)-1 ];
                $end_host_num = explode('.',$end_ip);
                $end_host_num = $end_host_num[ sizeof($end_host_num)-1 ];
                $start_host_num = (int)$start_host_num;
                $end_host_num = (int)$end_host_num;
                
                //break ip that request 
                $parts = explode('.',$request_ip);

                $request_ip_host_num = $parts[sizeof($parts) - 1 ];
                $request_ip_head = $parts[0] . '.'. $parts[1] .'.'. $parts[2];
                //check if request ip in range of the list
                if($request_ip_head == $ip_head && $request_ip_host_num >= $start_host_num && $request_ip_host_num <= $end_host_num){
                    
                    return $next($request);

                }
            }else if(trim($request_ip) == trim($value)){
                return $next($request);
            }
        }
        if( sizeof($allowed_ips) == 0  ){
            return $next($request);
        }
        else{
            abort(403,'Permission Denied');
        }
        
    }
}
