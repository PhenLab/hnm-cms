<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiSession extends Model
{
    //
    protected $fillable = [
        'desc',
        'user_id',
        'api_token',
        'type',

    ];
    
    public function user(){
        return $this->belongsTo('\App\User','user_id');
    }
}
